# sctl.py

**sctl** is a *wrapper* for *supervisorctl* because we use more then one supervisord.

Having more then 400 instances to control i needed to split the instances over different daemons.

The wrapper finds the config file for "enviroment (env)" and use that config for the supervisord that has the control for that env.

The input:

    ./sctl.py [action] [env]
    ./sctl.py restart foo_bar

  will execute

    supervisorctl -c /etc/supervisor/supervisord4.conf restart foo_bar:

**Action** can be:

- start
- stop
- restart
- status

We have the app config files in:

```
    /etc/supervisor/conf.d/*
    /etc/supervisor/conf.d2/*
    /etc/supervisor/conf.d3/*
    /etc/supervisor/conf.d4/*
```

and the supervisor configs are

```
    /etc/supervisor/supervisord.conf
    /etc/supervisor/supervisord2.conf
    /etc/supervisor/supervisord3.conf
    /etc/supervisor/supervisord4.conf
```

---

License

See the LICENSE file for license rights and limitations (MIT).