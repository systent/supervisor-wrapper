#!/usr/bin/python3
# sctl is a wrapper for supervisorctl because we use more then one supervisord.
# Having more then 400 instances to control i needed to split the instances over different daemons.
# The wrapper finds the config file for "enviroment (env)" and use that config for the supervisord
# that has the control for that env.
# The input:
#     ./sctl.py [action] [env]
#     ./sctl.py restart foo_bar
#   will execute
#     supervisorctl -c /etc/supervisor/supervisord4.conf restart foo_bar:
#
# Action can be:
#     - start
#     - stop
#     - restart
#     - status
#
# We have the app config files in:
#     /etc/supervisor/conf.d/*
#     /etc/supervisor/conf.d2/*
#     /etc/supervisor/conf.d3/*
#     /etc/supervisor/conf.d4/*
# and the supervisor configs are
#     /etc/supervisor/supervisord.conf
#     /etc/supervisor/supervisord2.conf
#     /etc/supervisor/supervisord3.conf
#     /etc/supervisor/supervisord4.conf
#
# Copyright (c) 2017, Claudio Mike Hofer

import sys
import argparse
import glob
import subprocess

SUPERVISORCTL_APP = '/usr/local/bin/supervisorctl'
SUPERVISOR_BASE_CONF_PATH = '/etc/supervisor/'
REPLACE_FROM = '{}conf.d'.format(SUPERVISOR_BASE_CONF_PATH)
REPLACE_TO = '{}supervisord'.format(SUPERVISOR_BASE_CONF_PATH)
APPEND_ENV_CONF = '.conf'
APPEND_CONF = '.conf'


def find_config(env=None):
    """search the conifg file for the enviroment and returns correspondig supervisord config file
        example env file will be:
            /etc/supervisor/conf.d4/foo_bar.conf
        example returns / converts to supervisor config:
            /etc/supervisor/supervisord4.conf

        :param env: the enviroment to find

        :return: the supervisord config file or None if not found
    """
    env = env.split(':')[0]
    search_pattern = '{}**/{}{}'.format(SUPERVISOR_BASE_CONF_PATH, env, APPEND_ENV_CONF)
    files = [file for file in glob.glob(search_pattern, recursive=True)]
    if files and len(files) == 1:
        config = '/'.join(files[0].split('/')[:-1])
        config = config.replace(REPLACE_FROM, REPLACE_TO)
        config = '{}{}'.format(config, APPEND_CONF)
        return config
    return None


def execute_supervisorctl(env=None, action=None, config=None):
    """run supervisorctl -c /etc/supervisor/supervisord3.conf restart cmh:

        :param env: enviroment name
        :param action: supervisor action, can only be: start, stop, restart
        :param config: supervisor config file
    """
    # subprocess.call(
    #     ['/usr/local/bin/supervisorctl', '-c {}'.format(config), action, '{}:'.format(env)],
    #     shell=True
    # )
    if ':' not in env:
        env = '{}:'.format(env)
    cmd = '{app} -c {config} {action} {env}'.format(app=SUPERVISORCTL_APP, config=config,
                                                    action=action, env=env)
    subprocess.call(cmd, shell=True)
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help='commands')

    # The start command
    list_parser = subparsers.add_parser('start', help='start environment')
    list_parser.add_argument('start_environment', action='store', help='Environment name')
    # The stop command
    list_parser = subparsers.add_parser('stop', help='stop environment')
    list_parser.add_argument('stop_environment', action='store', help='Environment name')
    # The restart command
    list_parser = subparsers.add_parser('restart', help='restart environment')
    list_parser.add_argument('restart_environment', action='store', help='Environment name')
    # The status command
    list_parser = subparsers.add_parser('status', help='status environment')
    list_parser.add_argument('status_environment', action='store', help='Environment name')
    args = parser.parse_args()
    if 'start_environment' in args:
        action = "start"
        env = args.start_environment
    elif 'stop_environment' in args:
        action = "stop"
        env = args.stop_environment
    elif 'restart_environment' in args:
        action = "restart"
        env = args.restart_environment
    elif 'status_environment' in args:
        action = "status"
        env = args.status_environment
    else:
        print(parser.print_help())
        sys.exit(2)

    print("{} enviroment: '{}'".format(action.title(), env))
    config = find_config(env)
    if config:
        execute_supervisorctl(env, action, config)
    else:
        print("Config not found for env: {}".format(env))
        sys.exit(2)

    print("DONE")
